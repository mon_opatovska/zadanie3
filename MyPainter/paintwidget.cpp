#include "paintwidget.h"
#include <math.h>


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName, "png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline = (QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
		scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
	for (int j = 0; j < image.height(); j++)
	{
	QColor farba = image.pixelColor(i, j);
	farba.setRed(255 - farba.red());
	farba.setBlue(255 - farba.blue());
	farba.setGreen(255 - farba.green());
	image.setPixelColor(i, j, farba);
	}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
	outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
	outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
	for (int j = 0; j < y; j++)
	{
	QRgb farba = image.pixel(j, i);
	image.setPixel(j, i, 16777215 - farba);
	}
	}*/
	update();
}

void PaintWidget::medianFilter()
{

	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine() / 4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok + xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y - j - 1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		lastPoint = event->pos();
		body.append(lastPoint);
		painting = true;
	}
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());


	if (event->buttons() & Qt::RightButton)
	{
		int prvyx = body[0].x();
		int prvyy = body[0].y();

		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() - (prvyx - event->pos().x()), body[i].y() - (prvyy - event->pos().y())));
		}

		this->clearImage();
		this->Hranica();
	
	}

}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) 
	{
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	//QPainter painter(&image);

//	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//painter.drawLine(lastPoint, endPoint);
	//painter.drawPoint(lastPoint);
	//painter.drawPoint(endPoint);
	//modified = true;

	//int rad = (myPenWidth / 2) + 2;
	//update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));

	//lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::DDA(QPainter *painter, int x1, int y1, int x2, int y2)
{
	int xx, yy;
	double m;

	if (x2 < x1)
	{
		xx = x2;
		x2 = x1;
		x1 = xx;
		yy = y2;
		y2 = y1;
		y1 = yy;
	}

	double x = (double)x1;
	double y = (double)y1;

	int dx = fabs(x2 - x1);
	int dy = fabs(y2 - y1);

	if (dx >= dy)
		m = (double)dy / (double)dx;

	else
		m = (double)dx / (double)dy;

	do
	{
		if (y1 > y2)
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + 1;
				y = y - m;
			}

			else
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + m;
				y = y - 1;
			}
		}

		else
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + 1;
				y = y + m;
			}

			else
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + m;
				y = y + 1;
			}
		}
	} while ((int)round(x) != x2 || (int)round(y) != y2);

	painter->drawPoint(x2, y2);
}


void PaintWidget::Hranica()
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(myPenColor));

	for (int i = 0; i < body.size() - 1; i++)
		this->DDA(&painter, body.at(i).x(), body.at(i).y(), body.at(i + 1).x(), body.at(i + 1).y());

	this->DDA(&painter, body.at(0).x(), body.at(0).y(), body.at(body.size() - 1).x(), body.at(body.size() - 1).y());
}

void PaintWidget::Skalovanie(double parameter)
{
	for (int i = 0; i < body.size(); i++)
	{
		body.replace(i, QPoint(body[0].x() + (int)round(((double)body[i].x() - (double)body[0].x()) * parameter),
								body[0].y() + (int)round(((double)body[i].y() - (double)body[0].y()) * parameter)));
	}

	this->clearImage();
	
	this->Hranica();
}

void PaintWidget::Otacanie(bool smer)
{
	double uhol = M_PI / 12.;
	
	if (smer == 0)
	{
		for (int i = 0; i < body.size(); i++)
		{
			double dlzkax = (double)body[i].x() - (double)body[0].x();
			double dlzkay = (double)body[i].y() - (double)body[0].y();

			body.replace(i, QPoint((int)round((dlzkax * cos(uhol)) - dlzkay * sin(uhol)) + body[0].x(),
				body[0].y() + (int)round((dlzkax * sin(uhol)) + dlzkay * cos(uhol))));
		}
	}

	if (smer == 1)
	{
		for (int i = 0; i < body.size(); i++)
		{
			double dlzkax = (double)body[i].x() - (double)body[0].x();
			double dlzkay = (double)body[i].y() - (double)body[0].y();

			body.replace(i, QPoint((int)round((dlzkax * cos(uhol)) + dlzkay * sin(uhol)) + body[0].x(),
				body[0].y() + (int)round((-dlzkax * sin(uhol)) + dlzkay * cos(uhol))));
		}
		
	}
	
	this->clearImage();

	this->Hranica();
}

void PaintWidget::Preklopenie(bool os)
{
	if (os == 0)
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() + 2 * (body[0].x() - body[i].x()), body[i].y()));
		}
	}

	if (os == 1)
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x(), body[i].y() + 2 * (body[0].y() - body[i].y())));
		}
	}
	
	this->clearImage();

	this->Hranica();
}

void PaintWidget::Skosenie(bool os)
{
	double d = 0.5;
	
	if (os == 1) //os x
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() - d * (body[0].y() - body[i].y()), body[i].y()));
		}
	}

	if (os == 0) // os y
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x(), body[i].y() + d * (body[i].x() - body[0].x())));
		}
	}

	this->clearImage();

	this->Hranica();
}

void PaintWidget::Scan_line(int red_v, int green_v, int blue_v)
{
	QPainter painter(&image);
	painter.setPen(QPen(QColor(red_v, green_v, blue_v), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor(red_v, green_v, blue_v)));

	int j = 0;
	double dx, dy;

	for (int i = 0; i < body.size(); i++)
	{
		j++;

		if (i == body.size() - 1)
			j = 0;

		dx = body.at(j).x() - (double)body.at(i).x();
		dy = body.at(i).y() - (double)body.at(j).y();

		if (dy != 0) // vynecha vodorovne hrany
		{
			if (dy > 0) // rastuca
			{
				vektor.push_back((double)body.at(i).x());
				vektor.push_back((double)body.at(i).y());
				vektor.push_back((double)body.at(j).y());
			}

			else // klesajuca
			{
				vektor.push_back((double)body.at(j).x());
				vektor.push_back((double)body.at(j).y());
				vektor.push_back((double)body.at(i).y());
			}

			vektor.push_back(dx / dy);

			th.push_back(vektor); // priradenie do tabulky hran
		}

		vektor.clear(); // vyprazdnenie pomocneho vektora
	}

	for (int i = 0; i < th.size() - 1; i++) //triedenie podla yz
	{
		QVector <double> pom;

		for (int k = 0; k < th.size() - 1; k++)
		{
			if (th.at(k + 1).at(1) > th.at(k).at(1))
			{
				pom = th[k + 1];
				th[k + 1] = th[k];
				th[k] = pom;
			}
		}
	}


	for (int i = 0; i < th.size() - 1; i++) //triedenie podla xz
	{
		QVector <double> pom;

		for (int k = 0; k < th.size() - 1; k++)
		{
			if ((th.at(k + 1).at(1) == th.at(k).at(1)) && (th.at(k + 1).at(0) < (th.at(k).at(0))))
			{
				pom = th[k + 1];
				th[k + 1] = th[k];
				th[k] = pom;
			}
		}
	}

	for (int i = 0; i < th.size() - 1; i++) //triedenie podla w
	{
		QVector <double> pom;

		for (int k = 0; k < th.size() - 1; k++)
		{
			if ((((th.at(k + 1).at(1) == th.at(k).at(1)) && (th.at(k + 1).at(0) == (th.at(k).at(0)))))
				&& (th.at(k + 1).at(3) < (th.at(k).at(3))))
			{
				pom = th[k + 1];
				th[k + 1] = th[k];
				th[k] = pom;
			}
		}
	}

	for (int i = 0; i < th.size(); i++)
	{
		th[i][2] = th.at(i).at(2) + 1.0; // vymazanie posledneho pixelu

		printf("TH\n%d\t%d\t%d\t%.2lf\n", (int)th.at(i).at(0), (int)th.at(i).at(1), (int)th.at(i).at(2), th.at(i).at(3));
	}

	double ya = th.at(0).at(1);

	while (ya >= th[th.size() - 1][2])
	{
		for (int i = 0; i < th.size(); i++)
		{
			if (ya == th[i][1])
			{
				printf("i - ya: %d - %.2lf\n", i, ya);
				tah.push_back(th[i]);


				for (int j = 0; j < tah.size(); j++)
					printf("TAH\n%d\t %d\t %d\t %.2lf\n", (int)tah[j][0], (int)tah[j][1], (int)tah[j][2], tah[j][3]);
			}

		}

		for (int i = 0; i < tah.size() - 1; i++) //triedenie podla yz
		{
			QVector <double> pom;

			for (int k = 0; k < tah.size() - 1; k++)
			{
				if (tah.at(k + 1).at(1) > tah.at(k).at(1))
				{
					pom = tah[k + 1];
					tah[k + 1] = tah[k];
					tah[k] = pom;
				}
			}
		}

		for (int i = 0; i < tah.size() - 1; i++) //triedenie podla xz
		{
			QVector <double> pom;

			for (int k = 0; k < tah.size() - 1; k++)
			{
				if ((tah.at(k + 1).at(1) == tah.at(k).at(1)) && (tah.at(k + 1).at(0) < (tah.at(k).at(0))))
				{
					pom = tah[k + 1];
					tah[k + 1] = tah[k];
					tah[k] = pom;
				}
			}
		}

		for (int i = 0; i < tah.size() - 1; i++) //triedenie podla w
		{
			QVector <double> pom;

			for (int k = 0; k < tah.size() - 1; k++)
			{
				if ((((tah.at(k + 1).at(1) == tah.at(k).at(1)) && (tah.at(k + 1).at(0) == (tah.at(k).at(0)))))
					&& (tah.at(k + 1).at(3) < (tah.at(k).at(3))))
				{
					pom = tah[k + 1];
					tah[k + 1] = tah[k];
					tah[k] = pom;
				}
			}
		}

		vektor.clear();

		for (int i = 0; i < tah.size(); i++)
			vektor.push_back(tah[i][3] * (tah[i][1] - ya) + tah[i][0]);

		for (int i = 0; i < tah.size() - 1; i += 2)
			painter.drawLine(vektor[i], ya, vektor[i + 1], ya);

		int poc;
		for (int i = 0; i < tah.size(); i++)
		{
			if (tah[i][2] == ya)
				poc++;

		}

		ya--;

		printf("%d\n", poc);


	}
}